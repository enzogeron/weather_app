package com.eg.weatherapp.interfaces

import com.eg.weatherapp.models.Weather
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface OpenWeatherMap {

    @GET("weather")
    fun getWeather(
        @Query("q") cityName: String?,
        @Query("appid") apiToken: String?,
        @Query("units") metrics: String = "metric"
    ): Call<Weather>

    @GET("forecast")
    fun getCityByLatLong(
        @Query("lat") latitude: String?,
        @Query("lon") longitude: String?,
        @Query("appid") apiToken: String?
    ): Call<Weather>

}