package com.eg.weatherapp

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class AddCityActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_city)

        val etCity: EditText = findViewById(R.id.etCity)
        val btnSave: Button = findViewById(R.id.btnSave)
        btnSave.setOnClickListener {
            val intent = Intent()
            val city = etCity.text.toString()
            if(!city.isNullOrEmpty()) {
                intent.putExtra("city", city)
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
        }
    }
}