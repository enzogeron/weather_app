package com.eg.weatherapp

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.preference.PreferenceManager
import com.eg.weatherapp.interfaces.OpenWeatherMap
import com.eg.weatherapp.models.City
import com.eg.weatherapp.models.Main
import com.eg.weatherapp.models.Weather
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {

    private val MY_PERMISSIONS_REQUEST_CODE = 33
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var cities = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        getLocationWithPermissionCheck()

        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        val gson = Gson()
        val json = sharedPreferences.getString("cities", "")
        val type = object: TypeToken<ArrayList<String>>() {}.type
        if(!json.isNullOrEmpty())
            cities = gson.fromJson(json, type)

        val adapter = ArrayAdapter(this, R.layout.item, cities)
        val listView: ListView = findViewById(R.id.lvCities)
        listView.adapter = adapter

        listView.onItemClickListener = object : AdapterView.OnItemClickListener {
            override fun onItemClick(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val nameCity = listView.getItemAtPosition(position) as String
                getTemperature(nameCity)
            }
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.addCity -> openAddCityActivity()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun openAddCityActivity() {
        val intent = Intent(this, AddCityActivity::class.java)
        startForResult.launch(intent)
    }

    private val startForResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if(result.resultCode === Activity.RESULT_OK) {
            val data: Intent? = result.data
            val city: String? = data?.getStringExtra("city")

            cities.add(city as String)

            val prefs = PreferenceManager.getDefaultSharedPreferences(baseContext)
            val editor = prefs.edit()
            val gson = Gson()
            val json = gson.toJson(cities)
            editor.putString("cities", json)
            editor.apply()
        }
    }

    private fun getNameCity(latitude: String, longitude: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://api.openweathermap.org/data/2.5/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val openWeatherMap: OpenWeatherMap = retrofit.create(OpenWeatherMap::class.java)

        val call: Call<Weather> = openWeatherMap.getCityByLatLong(
            latitude,
            longitude,
            "1c883138ecbd66441050bb8a4e462f47"
        )

        call.enqueue(object: Callback<Weather?> {
            override fun onResponse(call: Call<Weather?>, response: Response<Weather?>) {
                if(!response.isSuccessful()) {
                    return
                }

                val weather: Weather? = response.body()
                val city: City? = weather?.getCity()
                val nameCity = city?.getName().toString()

                showMessage("Estas en la ciudad: " + nameCity)
                getTemperature(nameCity)
            }

            override fun onFailure(call: Call<Weather?>, t: Throwable) {
            }
        })
    }

    private fun getTemperature(city: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://api.openweathermap.org/data/2.5/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val openWeatherMap: OpenWeatherMap = retrofit.create(OpenWeatherMap::class.java)

        val call: Call<Weather> = openWeatherMap.getWeather(
            city.lowercase(),
            "1c883138ecbd66441050bb8a4e462f47"
        )

        call.enqueue(object: Callback<Weather?> {
            override fun onResponse(call: Call<Weather?>, response: Response<Weather?>) {
                if(!response.isSuccessful()) {
                    showMessage("No fue posible obtener la temperatura de " + city)
                    return
                }

                val weather: Weather? = response.body()
                val main: Main? = weather?.getMain()
                val temp: String = main?.getTemp().toString()

                showMessage("Temperatura: " + temp + "°C")

            }

            override fun onFailure(call: Call<Weather?>, t: Throwable) {
            }
        })
    }

    private fun showMessage(msg: String) {
        Toast.makeText(applicationContext, msg, Toast.LENGTH_SHORT).show()
    }

    private fun getLocationWithPermissionCheck() {
        if(ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
        ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
        ) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION),
                MY_PERMISSIONS_REQUEST_CODE)
        } else {
            fusedLocationClient.lastLocation
                .addOnSuccessListener { location: Location? ->
                    if(location != null) {
                        getNameCity(location.latitude.toString(), location.longitude.toString())
                    }
                }
        }
    }

}