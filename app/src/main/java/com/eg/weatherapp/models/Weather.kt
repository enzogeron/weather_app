package com.eg.weatherapp.models

class Weather {

    private val main: Main? = null
    private val city: City? = null

    fun getMain(): Main? {
        return main
    }

    fun getCity(): City? {
        return city
    }

}