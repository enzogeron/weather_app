package com.eg.weatherapp.models

class Main {

    private val temp: Double? = null
    private val feelsLike: Double? = null
    private val tempMin: Double? = null
    private val tempMax: Double? = null
    private val pressure: Double? = null
    private val humidity: Double? = null

    fun getTemp(): Double? {
        return temp
    }

    fun getFeelsLike(): Double? {
        return feelsLike
    }

    fun getTempMin(): Double? {
        return tempMin
    }

    fun getTempMax(): Double? {
        return tempMax
    }

    fun getPressure(): Double? {
        return pressure
    }

    fun getHumidity(): Double? {
        return humidity
    }

}